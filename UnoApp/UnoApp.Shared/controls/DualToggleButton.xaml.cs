﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Controllo utente è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234236

namespace UnoApp.Shared.controls
{
    public sealed partial class DualToggleButton : UserControl
    {
        /*Per riattivare il button di added e removed che appare e scompare 
		 * impostare lo style in xaml e buttonright visible a false*/

        //Colori previsti per il controllo.
        private SolidColorBrush _added ;//blu
        private SolidColorBrush _notSelected ;//azzurro
        private SolidColorBrush _selected  ;//lime
        private SolidColorBrush _foregroudSelected  ;
        private bool disableEvents = false;

        public DualToggleButton()
        {
            InitializeComponent();


            _added = GetSolidColorBrush("#FF5D87CC");//blu
            _notSelected = GetSolidColorBrush("#FF9ABAED");//azzurro
            _selected = GetSolidColorBrush("#FFD8EA54");//lime
            _foregroudSelected = GetSolidColorBrush("#FF335C9F");
        }

        public SolidColorBrush GetSolidColorBrush(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            byte a = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte r = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
            SolidColorBrush myBrush = new SolidColorBrush(Windows.UI.Color.FromArgb(a, r, g, b));
            return myBrush;
        }

        // deseleziona i bottoni che hanno lo stesso parent, ma non questo
        public void deselectOtherButtons()
        {

            var parent = VisualTreeHelper.GetParent(this);
            int nc = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < nc; i++)
            {
                var b = VisualTreeHelper.GetChild(parent, i);

                if (b is DualToggleButton btn)
                {
                    if (btn != this)
                        btn.NotSelectedState();
                }
            }
        }

        // deseleziona i bottoni che hanno lo stesso parent

        public static void deselectAllButtons(UIElement parent)
        {

            int nc = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < nc; i++)
            {
                var b = VisualTreeHelper.GetChild(parent, i);

                if (b is DualToggleButton btn)
                {
                    btn.NotSelectedState(false);
                }
            }
        }

        public static DualToggleButton GetSelectedBaloon(UIElement theparent)
        {
            // Scorri gli elementi 
            // se il checkboxBallon contiene un elemento selezionato, ritorna l'oggetto


            foreach (DualToggleButton tb in helpers.Helper.FindVisualChildren<DualToggleButton>(theparent))
            {
                if (tb.IsSelected)
                    return tb;

            }

            return null;

        }

        private void OnPointEnter(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "VisualStateNormal", false);
        }

        private void OnPointExit(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "VisualStateAnimate", false);
        }
        private void UCToggleButton_Loaded(object sender, RoutedEventArgs e)
        {
            GetDefault();
        }
        private void UCToggleButton_LayoutUpdated(object sender, EventArgs e)
        {

        }

        //Imposta i valori di default del controllo.
        private void GetDefault()
        {
            txbTesto.Text = ToggleButtonText;

            if (Selectable && IsSelected)
                SelectedState();
            else if (Selectable && !IsSelected)
                NotSelectedState();

            if (IsAdded)
                AddedState();

            if (Countable)
            {
                CountableState(true);
            }
            else
            {
                txbCounter.Text = string.Empty;
                txbCounter.Visibility = Visibility.Collapsed;
            }
        }

        public void SelectedState()
        {
            if (Selectable)
            {
                txbTesto.Foreground = _foregroudSelected;
                rctToggleButton.Fill = rctToggleButton.Stroke = _selected;
                IsSelected = true;
            }
            if (Countable && TotalCount > 0)
            {
                AddedState();
                txbCounter.Text = string.Format($"{TotalCount:D}");
            }
        }

        public void NotSelectedState(bool raiseEvents = true)
        {
            disableEvents = !raiseEvents;
            txbTesto.Foreground = new SolidColorBrush(Windows.UI.Colors.White);
            rctToggleButton.Fill = rctToggleButton.Stroke = IsAdded ? _added : _notSelected;
            IsSelected = false;
            elpButtonThreeState.Fill = elpButtonThreeState.Stroke = _foregroudSelected;
            disableEvents = false;
        }

        public void AddedState()
        {
            if (Addable)
            {
                rctToggleButton.Fill = rctToggleButton.Stroke = IsSelected ? _selected : _added;
                txbTesto.Foreground = IsSelected ? _foregroudSelected : new SolidColorBrush(Windows.UI.Colors.White);
                // UWP TBD

                RotateTransform xx = new RotateTransform();
                xx.Angle = 45;
                rctHorizontal.RenderTransform = xx;
                rctVertical.RenderTransform = xx;

               // rctHorizontal.RenderTransform = new RotateTransform(45);
               //  rctVertical.RenderTransform = new RotateTransform(45);
               IsAdded = true;
            }
        }

        public void RemovedState()
        {
            rctToggleButton.Fill = rctToggleButton.Stroke = IsSelected ? _selected : _notSelected;
            txbTesto.Foreground = IsSelected ? _foregroudSelected : new SolidColorBrush(Windows.UI.Colors.White);
            // UWP TBD
            RotateTransform xx = new RotateTransform();
            xx.Angle = 0;
            rctHorizontal.RenderTransform = xx;
            rctVertical.RenderTransform = xx;
            // rctHorizontal.RenderTransform = new RotateTransform(0);
            // rctVertical.RenderTransform = new RotateTransform(0);
            disableEvents = true;
            IsAdded = false;
            disableEvents = false;
        }

        public void CountableState(bool showingCount)
        {
            btnThreeState.Style = (Style)(Resources["RemoveMouseOver"]);
            btnThreeState.Opacity = 100;
            rctHorizontal.Visibility = Visibility.Collapsed;
            rctVertical.Visibility = Visibility.Collapsed;
            if (showingCount && TotalCount > 0)
            {
                elpButtonThreeState.Fill = elpButtonThreeState.Stroke = _foregroudSelected;
                txbCounter.Text = string.Format("{0:0}", TotalCount);
                txbCounter.Visibility = Visibility.Visible;
                AddedState();
            }
            else
            {
                btnThreeState.Opacity = 0;
                RemovedState();
            }
        }

        //Imposta il colore del button come selezionato o non selezionto in base allo stato attuale.
        private void BtnToggleButton_Click(object sender, RoutedEventArgs e)
        {
            toggleSelected();
        }

        /// <summary>
        /// Modifica lo stato di selezionato del bottone
        /// </summary>
        private void toggleSelected()
        {
            if (!IsSelected)
                SelectedState();
            else if (IsSelected)
                NotSelectedState();
        }

        //Imposta il colore del rettangolo per button e, l'icona + o x in base allo
        //stato attuale del button principale.
        private void BtnThreeState_Click(object sender, RoutedEventArgs e)
        {
            SelectedState();
            toggleAdded();
        }

        private void toggleAdded()
        {
            if (!IsAdded)
                AddedState();
            else if (IsAdded)
                RemovedState();
        }

        #region PROPERTY DEPENDENCY

        /// <summary>
        /// Imposta il testo da visualizzare all'interno del ToggleButton.
        /// </summary>
        public string ToggleButtonText
        {
            get { return (string)GetValue(ToggleButtonTextProperty); }
            set { SetValue(ToggleButtonTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ToggleButtonText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ToggleButtonTextProperty =
            DependencyProperty.Register("ToggleButtonText", typeof(string), typeof(DualToggleButton), new PropertyMetadata(null));

        /// <summary>
        /// Imposta il toggle button come: selezionato (True), non selezionato (False)
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(DualToggleButton), new PropertyMetadata(false, new PropertyChangedCallback(onSelectedChanged)));

        private static void onSelectedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

            if (d is DualToggleButton btn)
            {
                if (btn.disableEvents)
                    return;
                // UWP TBD
                //btn.RaiseEvent(new StatusChangedEventArgs((bool)e.OldValue, (bool)e.NewValue, btn.IsAdded, btn.IsAdded));
            }
        }

        /// <summary>
        /// Imposta il toggle button come: stato di aggiunto (True), stato rimosso (False).
        /// </summary>
        public bool IsAdded
        {
            get { return (bool)GetValue(IsAddedProperty); }
            set { SetValue(IsAddedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsAdded.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsAddedProperty =
            DependencyProperty.Register("IsAdded", typeof(bool), typeof(DualToggleButton), new PropertyMetadata(false, new PropertyChangedCallback(onAddedChanged)));

        private static void onAddedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // UWP TBD

            //if (d is DualToggleButton btn)
            //    if (!btn.disableEvents)
            //        btn.RaiseEvent(new StatusChangedEventArgs(btn.IsSelected, btn.IsSelected, (bool)e.OldValue, (bool)e.NewValue));
        }

        /// <summary>
        /// Imposta il controllo come: stato selezionabile (True).
        /// </summary>
        public bool Selectable
        {
            get { return (bool)GetValue(SelectableProperty); }
            set { SetValue(SelectableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Selectable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectableProperty =
            DependencyProperty.Register("Selectable", typeof(bool), typeof(DualToggleButton), new PropertyMetadata(true));

        /// <summary>
        /// Imposta il controllo come: stato aggiungibile (True).
        /// </summary>
        public bool Addable
        {
            get { return (bool)GetValue(AddableProperty); }
            set { SetValue(AddableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Selectable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AddableProperty =
            DependencyProperty.Register("Addable", typeof(bool), typeof(DualToggleButton), new PropertyMetadata(true));

        /// <summary>
        /// Imposta il controllo come: contatore, al posto del button di aggiunta o rimozione,
        /// nel lato destro del controllo stesso.
        /// </summary>
        public bool Countable
        {
            get { return (bool)GetValue(CountableProperty); }
            set { SetValue(CountableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Countable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CountableProperty =
            DependencyProperty.Register("Countable", typeof(bool), typeof(DualToggleButton), new PropertyMetadata(false));

        /// <summary>
        /// Restituisce il valore totale, se il controllo è stato impostato come Countable. 
        /// </summary>
        public int TotalCount
        {
            get { return (int)GetValue(TotalCountProperty); }
            set { SetValue(TotalCountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TotalCount.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TotalCountProperty =
            DependencyProperty.Register("TotalCount", typeof(int), typeof(DualToggleButton), new PropertyMetadata(0));                 // UWP TBD
                                                                                                                                           // {  PropertyChangedCallback = new PropertyChangedCallback(onTotalCountChanged) });

        private static void onTotalCountChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var me = d as DualToggleButton;
            me.CountableState(true);
        }

        /// <summary>
        /// Mostra o nasconde il button per lo stato: added o removed
        /// </summary>
        public bool ButtonRightVisible
        {
            get => (bool)GetValue(ButtonRightVisibleProperty);
            set => SetValue(ButtonRightVisibleProperty, value);
        }

        // Using a DependencyProperty as the backing store for ButtonRightVisible.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ButtonRightVisibleProperty =
            DependencyProperty.Register("ButtonRightVisible", typeof(bool), typeof(DualToggleButton), new PropertyMetadata(true, new PropertyChangedCallback(onButtonRightVisibleChange)));

        private static void onButtonRightVisibleChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var me = d as DualToggleButton;

            bool visible = (bool)e.NewValue;
            me.btnThreeState.Visibility = visible ? Visibility.Visible : Visibility.Collapsed;

            var margin = me.txbTesto.Margin;
            margin.Right = visible ? 28 : 12;
            me.txbTesto.Margin = margin;
        }

        //Valutare se mantenere questa dp
        public KeyValuePair<string, string> ToggleButtonKeyValue
        {
            get { return (KeyValuePair<string, string>)GetValue(ToggleButtonKeyValueProperty); }
            set { SetValue(ToggleButtonKeyValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ToggleButtonKeyValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ToggleButtonKeyValueProperty =
            DependencyProperty.Register("ToggleButtonKeyValue", typeof(KeyValuePair<string, string>), typeof(DualToggleButton),
                new PropertyMetadata(new KeyValuePair<string, string>()) ); // { DefaultValue = new KeyValuePair<string, string>() });

        #endregion

        #region events

        public sealed class StatusChangedEventArgs : RoutedEventArgs
        {
            public enum WhichStatusChanged { Added, Selected, Both };

            public bool oldSelected;
            public bool newSelected;
            public bool oldAdded;
            public bool newAdded;

            // UWP TBD
            public StatusChangedEventArgs(bool os, bool ns, bool oa, bool na) // : base(statusChangedChangedEvent)
            {
                oldSelected = os;
                newSelected = ns;
                oldAdded = oa;
                newAdded = na;
            }

            public WhichStatusChanged whichStatusIsChanged()
            {
                if (oldSelected != newSelected && oldAdded != newAdded)
                    return WhichStatusChanged.Both;
                else if (oldSelected != newSelected)
                    return WhichStatusChanged.Selected;
                else
                    return WhichStatusChanged.Added;
            }

        }

        public delegate void statusChangedEventHandler(object sender, StatusChangedEventArgs args);


        // UWP TBD
        /*
        public static readonly RoutedEvent statusChangedChangedEvent =
            EventManager.RegisterRoutedEvent("statusChangedChanged", RoutingStrategy.Bubble, typeof(EventHandler<StatusChangedEventArgs>), typeof(DualToggleButton));

        public event EventHandler<StatusChangedEventArgs> statusChanged
        {
            add { AddHandler(statusChangedChangedEvent, value); }
            remove { RemoveHandler(statusChangedChangedEvent, value); }
        }
        */

        #endregion

        //private KeyValuePair<string, string> _ToggleButtonKeyValue;
        //public KeyValuePair<string, string> ToggleButtonKeyValue { get => _ToggleButtonKeyValue; set => _ToggleButtonKeyValue = value; }
    } 
}

